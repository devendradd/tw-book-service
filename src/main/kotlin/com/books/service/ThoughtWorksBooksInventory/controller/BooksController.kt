package com.books.service.ThoughtWorksBooksInventory.controller

import com.books.service.ThoughtWorksBooksInventory.dto.Books
import com.books.service.ThoughtWorksBooksInventory.service.BookService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/v1")
class BooksController {

    @Autowired
    lateinit var bookService: BookService

    @GetMapping("/greetings")
    fun greeting() : String{
        return  "Hello from Kotlin"
    }

    //done
    @GetMapping("/getAllBooks")
    fun getAllBooks(): List<Books>{
        return bookService.getAllBooks()
    }

    //done
    @PostMapping("/addBook")
    fun addBook(@ModelAttribute book: Books){
        return bookService.saveBook(book)
    }



}