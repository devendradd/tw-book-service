package com.books.service.ThoughtWorksBooksInventory

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ThoughtWorksBooksInventoryApplication

fun main(args: Array<String>) {
	runApplication<ThoughtWorksBooksInventoryApplication>(*args)
}
