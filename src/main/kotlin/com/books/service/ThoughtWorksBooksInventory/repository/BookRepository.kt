package com.books.service.ThoughtWorksBooksInventory.repository

import com.books.service.ThoughtWorksBooksInventory.dto.Books
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface BookRepository : MongoRepository<Books, Int> {

}