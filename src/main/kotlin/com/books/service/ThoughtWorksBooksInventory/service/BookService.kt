package com.books.service.ThoughtWorksBooksInventory.service

import com.books.service.ThoughtWorksBooksInventory.dto.Books
import com.books.service.ThoughtWorksBooksInventory.repository.BookRepository
import org.springframework.stereotype.Service

@Service
class BookService(var bookRepository : BookRepository) {

    fun getAllBooks(): List<Books>{
        return bookRepository.findAll()
    }

    fun saveBook(book : Books) {
        bookRepository.save(book)
    }


}



//    var b1 = Books(121,"DD", "DD", "my image", "desc", 123.23, 2)
//    var b2 = Books(1212,"DD", "DD", "my image", "desc", 123.23, 2)
//
//    fun getBooks(): ArrayList<Books>{
//        var lst : ArrayList<Books> = ArrayList<Books>()
//        lst.add(b1)
//        lst.add(b2)
//        return lst
//    }