package com.books.service.ThoughtWorksBooksInventory.dto

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable
import java.math.BigInteger

@Document
data class Books(@Id var _id: BigInteger,
                 var title: String,
                 var authors: String,
                 var image: String,
                 var description: String,
                 var price: Double,
                 var quantity: Int) : Serializable